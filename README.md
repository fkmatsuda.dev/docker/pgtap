# pgtap

pgtap docker image

## Name
PGTAP Docker Image

## Description
A docker image from the official postgres alpine image with the pgtap extension and the pg_prove utility and junit formatter

## Usage
You can use the [fk_pgmigration](https://gitlab.com/fkmatsuda.dev/go/fk_pgmigration) project as an example usage...
## Support
You can ask me for help by [e-mail](mailto:fabio@fkmatsuda.dev), I will try to help as best I can.

## Roadmap
I will try to keep this project up to date, with a tag for each version of postgres from the current 13.3 at the time I started the project.

## License
This container container image contains:
* PostgreSQL is released under the [PostgreSQL License](./licenses/postgresql.md).
* pgTAP and pg_prove have a permissive license by [David E. Wheeler](./licenses/pgtap.md).
* Perl has a Permissive License by [Larry Wall and others](./licenses/perl.md).
* TAP::Formatter::JUnit follow the same terms as the Perl license by [Graham TerMarsch](./licenses/tap_formatter_junit.md).

Other licenses may be applicable, I believe they are all open source, please excuse me for the lack of reference for all.
