FROM postgres:13.3-alpine3.14

ENV TAP_VERSION 1.1.0

RUN apk add --no-cache unzip perl perl-dev perl-utils perl-module-build expat expat-dev libxml2 libxml2-dev patch build-base; \
    wget -O pgtap.zip "https://api.pgxn.org/dist/pgtap/$TAP_VERSION/pgtap-$TAP_VERSION.zip"; \
    unzip pgtap.zip; \
    rm pgtap.zip; \
    mv pgtap-${TAP_VERSION} pgtap; \
    cd pgtap; \
    make; \
    make install; \
    cd ..; \
    rm -rf pgtap; \
    cpan App::cpanminus; \
    cpanm TAP::Parser::SourceHandler::pgTAP --no-wget; \
    cpanm TAP::Formatter::JUnit --no-wget; \
    rm -rf /root/.cpan; \
    rm -rf /root/.cpanm; \
    apk del --no-cache unzip perl-dev perl-utils perl-module-build expat-dev libxml2-dev patch build-base;

COPY start-pg.sh /usr/local/bin/
