#!/bin/bash
docker-entrypoint.sh postgres &

RETRIES=10

until psql -h $PG_HOST -U $PG_USER -d $PG_DATABASE -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server, $((RETRIES--)) remaining attempts..."
  sleep 0.25s
done

su - postgres -c 'psql -h localhost -U postgres -d template1 -c "CREATE EXTENSION IF NOT EXISTS pgtap;"'
